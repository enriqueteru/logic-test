

const checkPalindromous  = (word) => { 

   const reverse = word.split("").reverse().join("");
   return reverse === word ? console.log(`${word} is Palindromous word --->`, true) : console.log(`${word} is not a Palindromous word --->`, false);
   
}


checkPalindromous('jose');
checkPalindromous('ana');